# i3-locker

Program to start i3lock with a resized background image.

## Why C++ instead of (bash/python/etc.)?

Why not.

## Building

Requires a compiler that supports C++17. Tested on GCC 7.3 and Clang 5/6.

### Install Dependencies

- Microsoft Guidelines Support Library
- spdlog
  - fmt
- catch2
- libdbus
- ImageMagick-c++
- XCB

Run `./linux-build-dependencies.sh` to auto-download dependencies.

### Generate Build Files

- If using manually installed packages, make sure to set `_DIR` vars to point to them (e.g. `-Dfmt_DIR=/path/to/fmtConfig.cmake`).
    - `./linux-build-dependencies.sh` will print out the necessary CMake variables (if used).
- Set `-Dmsgsl_INCLUDE_DIRS`

```sh
$ cd <project_root>
$ mkdir build && cd build && cmake -Dmsgsl_INCLUDE_DIRS="/path/to/msgsl/include" -DCMAKE_BUILD_TYPE=Debug ..
```

### Compile

```sh
$ cd <project_root>/build
$ make -j$(nproc)
```
