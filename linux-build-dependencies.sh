#!/bin/sh

set -eu

usage() {
    echo "Usage: $0 [-S]" 1>&2
    echo "-S: Assume package install doesn't require sudo"
    echo "-O: Don't install OS packages"
    echo '-h: help'
    exit 1
}

sudo_cmd='sudo'
option_install_os_packages=true

while getopts ":SOh" o; do
    case "${o}" in
        S)
            sudo_cmd=''
            ;;
        O)
            option_install_os_packages=false
            ;;
        h)
            usage
            ;;
        \?)
            usage
            ;;
    esac
done

fmt_version='4.1.0'
spdlog_version='0.16.3'
catch_version='2.2.2'

debian_packages=(make cmake pkg-config libdbus-1-dev libx11-xcb-dev libcereal-dev libmagick++-dev)
redhat_packages=(make cmake pkgconf-pkg-config dbus-devel libxcb-devel cereal-devel ImageMagick-c++-devel)

function join_by { local IFS="$1"; shift; echo "$*"; }

function install_os_packages {
    all_os_packages_installed=true
    if [ -f  /etc/debian_version ]; then
        echo 'Debian based distro'
        if [ -f /usr/bin/apt ]; then
            echo 'Distro has apt'
            installed_packages=`apt list --installed`
            for package in "${debian_packages[@]}"
            do
                if ! echo "$installed_packages" | grep -q "$package"; then
                    echo "missing package: $package"
                    all_os_packages_installed=false
                fi
            done
            if [ "$all_os_packages_installed" = false ]; then
                echo 'Installing OS packages...'
                eval "$sudo_cmd" apt -y install `join_by ' ' "${debian_packages[@]}"`
            fi
        else
            echo 'Unknown package manager'
        fi
    elif [ -f /etc/redhat-release ]; then
        echo 'RedHat based distro'
        if [ -f /usr/bin/dnf ]; then
            echo 'Distro has dnf'
            installed_packages=`dnf list installed`
            for package in "${redhat_packages[@]}"
            do
                if ! echo "$installed_packages" | grep -q "$package"; then
                    echo "missing package: $package"
                    all_os_packages_installed=false
                fi
            done
            if [ "$all_os_packages_installed" = false ]; then
                echo 'Installing OS packages...'
                eval "$sudo_cmd" dnf -y install `join_by ' ' "${redhat_packages[@]}"`
            fi
        elif [ -f /usr/bin/yum ]; then
            echo 'Distro has yum'
            installed_packages=`yum list installed`
            for package in "${redhat_packages[@]}"
            do
                if ! echo "$installed_packages" | grep -q "$package"; then
                    echo "missing package: $package"
                    all_os_packages_installed=false
                fi
            done
            if [ "$all_os_packages_installed" = false ]; then
                echo 'Installing OS packages...'
                eval "$sudo_cmd" yum -y install `join_by ' ' "${redhat_packages[@]}"`
            fi
        else
            echo 'Unknown package manager'
        fi
    else
        echo 'Unknown distro'
    fi
}

if [ "$option_install_os_packages" = true ]; then
    install_os_packages
fi

# Final directory for dependencies
gsl_dir='vendor/GSL-master'
fmt_dir="vendor/fmt-$fmt_version"
spdlog_dir="vendor/spdlog-$spdlog_version"
catch_dir="vendor/Catch2-$catch_version"

gsl_url='https://github.com/Microsoft/GSL/archive/master.tar.gz'
fmt_url="https://github.com/fmtlib/fmt/archive/$fmt_version.tar.gz"
spdlog_url="https://github.com/gabime/spdlog/archive/v$spdlog_version.tar.gz"
catch_url="https://github.com/catchorg/Catch2/archive/v$catch_version.tar.gz"

mkdir -p vendor

if [ ! -d "$gsl_dir" ]; then
    curl -L $gsl_url | tar -xz -C vendor
fi
if [ ! -d "$fmt_dir" ]; then
    curl -L $fmt_url | tar -xz -C vendor
fi
if [ ! -d "$spdlog_dir" ]; then
    curl -L $spdlog_url | tar -xz -C vendor
fi
if [ ! -d "$catch_dir" ]; then
    curl -L $catch_url | tar -xz -C vendor
fi

cd "$fmt_dir" && mkdir -p build && cd build && cmake -DFMT_TEST=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..
cd "$spdlog_dir" && mkdir -p build && cd build && cmake -DSPDLOG_BUILD_TESTING=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..
cd "$catch_dir" && mkdir -p build && cd build && cmake -DBUILD_TESTING=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..

echo -n '-Dmsgsl_INCLUDE_DIRS='
find "$gsl_dir" -type d -name 'include' -exec readlink -f '{}' \; | tr '\n' ' '
echo -n '-Dfmt_DIR='
find "$fmt_dir/install" -type d -path '*/cmake/fmt' -exec readlink -f '{}' \; | tr '\n' ' '
echo -n '-Dspdlog_DIR='
find "$spdlog_dir/install" -type d -path '*/cmake/spdlog' -exec readlink -f '{}' \; | tr '\n' ' '
echo -n '-DCatch2_DIR='
find "$catch_dir/install" -type d -path '*/cmake/Catch2' -exec readlink -f '{}' \;
