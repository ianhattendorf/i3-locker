/*!
 * \file Util.cpp
 */

#include "libutil/Util.hpp"
#include <pwd.h>
#include <stdexcept>
#include <unistd.h>

std::string Util::get_user_home_dir() {
  // First check user provided $HOME env
  const char *const user_home_env = std::getenv("HOME");
  if (user_home_env != nullptr) {
    return user_home_env;
  }

  // If $HOME not provided, use getpwuid
  const passwd *const pw = getpwuid(getuid());
  if (pw == nullptr) {
    throw std::runtime_error("getpwuid returned nullptr");
  }

  return pw->pw_dir;
}

std::optional<long> Util::string_to_number(const std::string &string) {
  if (string.empty() || ((!isdigit(string[0])) && (string[0] != '-') && (string[0] != '+'))) {
    return {};
  }

  char *cstr_end;
  const long number = strtol(string.c_str(), &cstr_end, 10);

  if (*cstr_end != 0) {
    return {};
  }
  return number;
}
