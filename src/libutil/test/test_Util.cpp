/*!
 * \file test_Util.cpp
 * \brief Tests for Util.hpp
 */

#include "libutil/Util.hpp"
#include <catch.hpp>
#include <limits>
#include <string>

TEST_CASE("Util::string_to_number", "[Util]") {
  REQUIRE(Util::string_to_number("0") == 0);
  REQUIRE(Util::string_to_number("1") == 1);
  REQUIRE(Util::string_to_number("-1") == -1);
  REQUIRE(Util::string_to_number("123") == 123);
  REQUIRE(Util::string_to_number("-123") == -123);
  REQUIRE(Util::string_to_number(std::to_string(std::numeric_limits<long>::max())) ==
          std::numeric_limits<long>::max());
  REQUIRE(Util::string_to_number(std::to_string(std::numeric_limits<long>::min())) ==
          std::numeric_limits<long>::min());

  REQUIRE_FALSE(Util::string_to_number(" 123 ").has_value());
  REQUIRE_FALSE(Util::string_to_number(" -123 ").has_value());
  REQUIRE_FALSE(Util::string_to_number("f123").has_value());
  REQUIRE_FALSE(Util::string_to_number("12f3").has_value());
  REQUIRE_FALSE(Util::string_to_number("f-123").has_value());
  REQUIRE_FALSE(Util::string_to_number("123f").has_value());
  REQUIRE_FALSE(Util::string_to_number("-123f").has_value());
}
