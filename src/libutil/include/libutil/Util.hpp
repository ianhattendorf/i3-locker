/*!
 * \file Util.hpp
 */

#pragma once

#include <optional>
#include <string>

class Util {
public:
  static std::string get_user_home_dir();
  static std::optional<long> string_to_number(const std::string &string);
};
