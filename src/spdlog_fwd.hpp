/*!
 * \file spdlog_fwd.hpp
 * \brief Forward declarations for spdlog
 */

#pragma once

namespace spdlog {
class logger;
}
