/*!
 * \file App.cpp
 */

#include "App.hpp"
#include <Magick++.h>
#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/utility.hpp>
#include <dbus/dbus.h>
#include <experimental/filesystem>
#include <fstream>
#include <gsl/gsl_util>
#include <libutil/Util.hpp>
#include <sys/stat.h>
#include <sys/wait.h>
#include <thread>
#include <xcb/xcb.h>

struct ImageProperties {
  std::string image_source_path;
  std::pair<int, int> dimensions;

  bool operator==(const ImageProperties &rhs) const {
    return image_source_path == rhs.image_source_path && dimensions == rhs.dimensions;
  }

  bool operator!=(const ImageProperties &rhs) const { return !(rhs == *this); }

  template <class Archive> void serialize(Archive &archive) {
    archive(image_source_path, dimensions);
  }
};

using namespace std::string_literals;
namespace fs = std::experimental::filesystem;

App::App(gsl::span<char *, gsl::dynamic_extent> args) {
  Magick::InitializeMagick(args[0]);
}

void App::run() {
  const std::string i3lock_path = "/usr/bin/i3lock";
  const std::string dunst_path = "/usr/bin/dunst";

  const std::string user_home_dir = Util::get_user_home_dir();
  const std::string source_wallpaper_path = user_home_dir + "/Pictures/wallpaper.png";
  const std::string dest_lockscreen_path = user_home_dir + "/.cache/i3lock/lockscreen_cached.png";
  const std::string cache_properties_path = user_home_dir + "/.cache/i3lock/.cache_properties";

  const std::pair<int, int> display_dimensions = get_display_dimensions();
  const ImageProperties current_image_properties{source_wallpaper_path, display_dimensions};

  const bool image_needs_update = [&] {
    std::ifstream is{cache_properties_path, std::ios::binary};
    // Check if .cache_properties exists
    if (!is) {
      logger()->debug("{} not found, updating lockscreen image", cache_properties_path);
      return true;
    }

    // Check if cached lockscreen image exists
    if (!fs::exists(dest_lockscreen_path)) {
      logger()->debug("{} not found, updating lockscreen image", dest_lockscreen_path);
      return true;
    }

    // Check if .cache_properties == current properties
    cereal::BinaryInputArchive iarchive{is};
    ImageProperties cached_image_properties;
    iarchive(cached_image_properties);
    const bool properties_equal = cached_image_properties == current_image_properties;
    if (properties_equal) {
      logger()->debug("{} same as current propreties, using cached image", cache_properties_path);
    } else {
      logger()->debug("{} differs from current propreties, updating lockscreen image",
                      cache_properties_path);
    }
    return !properties_equal;
  }();

  const std::string &i3_lockscreen_path = [&] {
    if (!image_needs_update) {
      return dest_lockscreen_path;
    }
    if (display_dimensions == std::pair{0, 0}) {
      return source_wallpaper_path;
    }
    if (resize_image(source_wallpaper_path, dest_lockscreen_path, display_dimensions)) {
      std::ofstream os{cache_properties_path, std::ios::binary};
      cereal::BinaryOutputArchive oarchive{os};
      oarchive(current_image_properties);
      return dest_lockscreen_path;
    }
    return source_wallpaper_path;
  }();

  const std::optional<int> dunst_pid = get_pid_by_name(dunst_path);

  const pid_t pid = fork();
  int status;

  switch (pid) {
  case -1: { // Fork error
    throw std::runtime_error("fork: "s + std::strerror(errno));
  }
  case 0: { // Child
    logger()->debug("Launching i3lock...");

#ifdef DEBUG_BIN_TRUE
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg, hicpp-vararg)
    execl("/bin/true", "/bin/true", static_cast<char *>(nullptr));
#endif
#ifdef DEBUG_BIN_SLEEP
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg, hicpp-vararg)
    execl("/bin/sleep", "/bin/sleep", "2", static_cast<char *>(nullptr));
#endif

    // Make sure i3lock doesn't fork (`-n` flag)
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg, hicpp-vararg)
    execl(i3lock_path.c_str(), i3lock_path.c_str(), "-n", "-t", "-i", i3_lockscreen_path.c_str(),
          "--timepos=110:h-70", "--datepos=135:h-45", "--clock", "--datestr",
          "Type password to unlock...", "--insidecolor=00000000", "--ringcolor=ffffffff",
          "--line-uses-inside", "--keyhlcolor=ebbe65ff", "--bshlcolor=ebbe65ff",
          "--separatorcolor=00000000", "--insidevercolor=00000000", "--insidewrongcolor=ebbe65ff",
          "--ringvercolor=ffffffff", "--ringwrongcolor=ffffffff", "--indpos=x+280:h-70",
          "--radius=20", "--ring-width=4", "--veriftext=", "--wrongtext=", "--verifcolor=ffffffff",
          "--timecolor=ffffffff", "--datecolor=ffffffff", "--noinputtext=", "--force-clock",
          static_cast<char *>(nullptr));

    // If execl returns there was an error
    throw std::runtime_error("execl: "s + std::strerror(errno));
  }
  default: { // Parent, pid = child pid
    // Enable compton fading
    toggle_compton_fading(true);
    // Pause dunst notifications
    send_signal(dunst_pid, SIGUSR1);

    const auto finally_fading_disable = gsl::finally([] {
    // Sleep to allow fade to finish
#ifndef DEBUG_NO_FADE_SLEEP
      std::this_thread::sleep_for(std::chrono::milliseconds(400));
#endif
      // Disable compton fading
      toggle_compton_fading(false);
    });
    const auto finally_dunst_resume = gsl::finally([dunst_pid] {
      // Resume dunst notifications
      send_signal(dunst_pid, SIGUSR2);
    });

    // Wait for child to exit
    while (waitpid(pid, &status, 0) == -1) {
      switch (errno) {
      case EINTR:
        logger()->debug("waitpid EINTR");
        break;
      default:
        throw std::runtime_error("waitpid: "s + std::strerror(errno));
      }
    }

    // NOLINTNEXTLINE(hicpp-signed-bitwise)
    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
      throw std::runtime_error(
          fmt::format("Process {} with pid {} failed, status: {}", i3lock_path, pid, status));
    }
  }
  }
}

spdlog::logger *App::logger() {
  static auto instance = spdlog::stdout_color_mt("App");
  return instance.get();
}

std::optional<int> App::get_pid_by_name(std::string_view name) {
  for (const fs::directory_entry &dir : fs::directory_iterator("/proc")) {
    const fs::path &path = dir.path();
    const auto pid = Util::string_to_number(path.filename().string());
    if (!pid.has_value()) {
      continue;
    }

    const fs::path cmdline_path = path / fs::path{"cmdline"};
    std::ifstream cmdline_file{cmdline_path};
    if (cmdline_file.fail()) {
      // If a process exited between the time we read the directory and now,
      // it isn't of interest to us anyway
      continue;
    }
    std::string cmdline_string;
    std::getline(cmdline_file, cmdline_string);
    if (cmdline_string.compare(0, name.length(), name) == 0) {
      // Assume just a single process exists...
      return pid;
    }
  }

  return {};
}

void App::send_signal(std::optional<pid_t> pid, int signal) {
  const char *const signal_name = strsignal(signal);

  if (!pid.has_value()) {
    logger()->info("Not sending {} to {}, process doesn't exist.", signal_name, pid.value());
    return;
  }

  logger()->debug("Sending {} to pid {}...", signal_name, pid.value());
  if (kill(pid.value(), signal) == -1) {
    logger()->warn("Failed sending {} to pid {}: {}", signal_name, pid.value(),
                   std::strerror(errno));
  } else {
    logger()->debug("Sent {} to pid {}.", signal_name, pid.value());
  }
}

void App::toggle_compton_fading(bool enable) {
  // Maybe use dbus-cpp/cxx instead

  // Get a connection to the session bus
  DBusError error;
  dbus_error_init(&error);
  DBusConnection *const connection = dbus_bus_get(DBUS_BUS_SESSION, &error);
  if (dbus_error_is_set(&error) != 0u || connection == nullptr) {
    logger()->warn("Failed to connect to DBus: {}", error.message);
    dbus_error_free(&error);
    return;
  }

  // Create method message
  // Hard-code display (`._0`)
  DBusMessage *message =
      dbus_message_new_method_call("com.github.chjj.compton._0", "/com/github/chjj/compton",
                                   "com.github.chjj.compton", "opts_set");
  Expects(message != nullptr);
  DBusMessageIter args;
  const char *const no_focus_string = "no_fading_openclose";
  const auto no_focus_bool = static_cast<int>(!enable);
  dbus_message_iter_init_append(message, &args);
  dbus_bool_t success = dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &no_focus_string);
  Expects(success);
  success = dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &no_focus_bool);
  Expects(success);

  // Send the method message
  DBusPendingCall *pending;
  success = dbus_connection_send_with_reply(connection, message, &pending, 1000);
  Expects(success);
  Expects(pending != nullptr);
  dbus_connection_flush(connection);
  dbus_message_unref(message);
  logger()->debug("Sent DBus message.");

  // Block and receive method response
  // TODO don't block
  dbus_pending_call_block(pending);
  message = dbus_pending_call_steal_reply(pending);
  Expects(message != nullptr);
  dbus_pending_call_unref(pending);
  if (dbus_message_get_type(message) == DBUS_MESSAGE_TYPE_ERROR) {
    logger()->warn("DBus error message: {}", dbus_message_get_error_name(message));
  }

  // Get return value from method response
  bool reply_success = false;
  if (dbus_message_iter_init(message, &args) == 0u) {
    logger()->warn("Message has no arguments");
  } else if (dbus_message_iter_get_arg_type(&args) != DBUS_TYPE_BOOLEAN) {
    logger()->warn("Argument is not a boolean");
  } else {
    dbus_message_iter_get_basic(&args, &reply_success);
  }
  dbus_message_unref(message);

  logger()->log(reply_success ? spdlog::level::debug : spdlog::level::warn, "DBus response: {}",
                reply_success);
}

bool App::resize_image(const std::string &source_path, const std::string &dest_path,
                       std::pair<int, int> dimensions) {
  Expects(dimensions.first > 0 && dimensions.second > 0);
  try {
    Magick::Image image;
    image.read(source_path);
    Magick::Geometry geom{static_cast<size_t>(dimensions.first),
                          static_cast<size_t>(dimensions.second)};
    geom.fillArea(true);
    image.resize(geom); // Maybe just sample?
    image.crop(geom);

    // Draw Rectangle
    const auto quantum_range = []() -> Magick::Quantum {
      using Magick::Quantum;
      return QuantumRange;
    }();
    image.fillColor(Magick::Color{0, 0, 0, static_cast<Magick::Quantum>(0.4 * quantum_range)});
    const Magick::DrawableRectangle time_rectangle{
        25.0, static_cast<double>(dimensions.second - 110), 325.0,
        static_cast<double>(dimensions.second - 30)};
    image.draw(time_rectangle);

    image.write(dest_path);
  } catch (const Magick::Exception &e) {
    logger()->warn("Failed to create lockscreen image: {}", e.what());
    return false;
  }
  return true;
}

std::pair<int, int> App::get_display_dimensions() {
  int preferred_screen_number;
  xcb_connection_t *const connection = xcb_connect(nullptr, &preferred_screen_number);
  const auto finally_xcb_disconnect = gsl::finally([connection] { xcb_disconnect(connection); });
  if (xcb_connection_has_error(connection) != 0) {
    logger()->warn("Error connecting to X Server");
    return {0, 0};
  }

  const xcb_setup_t *const setup = xcb_get_setup(connection);
  xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);

  for (int i = 0; i < preferred_screen_number; ++i) {
    xcb_screen_next(&iter);
    logger()->debug("Found screen: {}, dimensions: [{}, {}]", iter.data->root,
                    iter.data->width_in_pixels, iter.data->height_in_pixels);
  }

  const xcb_screen_t *const screen = iter.data;
  Expects(screen != nullptr);

  const std::pair<int, int> dimensions{screen->width_in_pixels, screen->height_in_pixels};
  logger()->debug("Selected screen: {}, dimensions: [{}, {}]", screen->root, dimensions.first,
                  dimensions.second);

  return dimensions;
}
