/*!
 * \file main.cpp
 * \brief Main function definition
 */

#include "App.hpp"
#include <gsl/span>
#include <spdlog/spdlog.h>

int main(int argc, char *argv[]) {
  const auto args = gsl::make_span(argv, argc);

#ifdef NDEBUG
  spdlog::set_level(spdlog::level::info);
#else
  spdlog::set_level(spdlog::level::debug);
#endif
  const auto logger = spdlog::stdout_color_mt("main");
  logger->debug("Executable location: {}", args[0]);

  try {
    App app{args};
    app.run();
  } catch (const std::exception &e) {
    logger->critical(e.what());
    return EXIT_FAILURE;
  }

  logger->debug("Success.");

  return EXIT_SUCCESS;
}
