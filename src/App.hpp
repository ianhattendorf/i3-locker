/*!
 * \file App.hpp
 */

#pragma once

#include <csignal>
#include <gsl/span>
#include <optional>
#include <spdlog/spdlog.h>
#include <string_view>

class App {
public:
  App(gsl::span<char *, gsl::dynamic_extent> args);
  void run();

private:
  static spdlog::logger *logger();

  static std::optional<int> get_pid_by_name(std::string_view name);
  static void send_signal(std::optional<pid_t> pid, int signal);

  /*!
   * Toggle compton fading
   * \param enable True to enable fading, false to disable
   */
  static void toggle_compton_fading(bool enable);

  static bool resize_image(const std::string &source_path, const std::string &dest_path,
                           std::pair<int, int> dimensions);
  static std::pair<int, int> get_display_dimensions();
};
