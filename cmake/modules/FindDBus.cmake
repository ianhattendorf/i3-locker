# Find DBus
#
# Defined targets:
#  DBus::DBus

find_package(PkgConfig REQUIRED)
pkg_check_modules(PC_DBUS dbus-1)

find_library(DBus_LIBRARY
        DOC "Path to DBus library."
        NAMES dbus-1
        HINTS ${PC_DBUS_LIBDIR}
            ${PC_DBUS_LIBRARY_DIRS})

find_path(DBus_INCLUDE_DIR
        DOC "Path to DBus include directory."
        NAMES dbus/dbus.h
        HINTS ${PC_DBUS_INCLUDEDIR}
            ${PC_DBUS_INCLUDE_DIRS})

get_filename_component(dbus_library_path ${DBus_LIBRARY} PATH)
find_path(DBus_arch_INCLUDE_DIR
        DOC "Path to DBus arch include directory."
        NAMES dbus/dbus-arch-deps.h
        HINTS ${PC_DBUS_INCLUDEDIR}
            ${PC_DBUS_INCLUDE_DIRS}
            ${dbus_library_path}
            ${DBUS_INCLUDE_DIR}
        PATH_SUFFIXES include)

set(DBus_INCLUDE_DIRS ${DBus_INCLUDE_DIR} ${DBus_arch_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(DBus REQUIRED_VARS DBus_INCLUDE_DIRS DBus_LIBRARY)

add_library(DBus::DBus UNKNOWN IMPORTED)
set_target_properties(DBus::DBus PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${DBus_INCLUDE_DIRS}"
        IMPORTED_LOCATION ${DBus_LIBRARY})
